const path = require('path')
const webpack = require('webpack')
const config = require('./ow.hybris.front.config')

// get this from a configuration file or something like that :)
const appName = config.projectName || 'custom'

console.log(`Using app name for bundle ${appName}`)

const configuration = {
  context: __dirname,
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].bundle.js'
  },


  resolve: {
    extensions: ['.js'],
    alias: {
      jquery: './responsive/common/js/jquery-2.1.1.min.js'
    }
  },

  module: {
    noParse: /jquery|lodash/,
    rules: [
      {
        test: new RegExp(`${appName}.*.(js)`).toString(),
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  }
}


// configure the project name with the given custom application name
configuration.entry = {}
configuration.entry[appName] = './ow.webpack.index.js'
// test: new RegExp(`(${appName}).*.(js)`).toString(),
console.log(`Configuration object`)
console.dir(configuration, { depth: null, colors: true })

module.exports = configuration;
