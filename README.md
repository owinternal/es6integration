
# About

This is just a PoC to make sure it is actually possible to include webpack within a hybris application

## Goals

* Integrate babel loader to compile the ES2015 code to a backwards-compatible code style to get the best of JS
* Integrate webpack to automate the loaders and minify scripts with a task manager to integrate it more easily within a Jenkins-like automatation sys
* Integrate eslint to make sure the best practices are being applied to the code and hence, assure a better performance / quality / scalability of the system itself

